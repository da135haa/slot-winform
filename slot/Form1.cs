﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace 拉霸
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (numericUpDown1.Value > int.Parse(labelShow.Text) || numericUpDown1.Value == 0)
            {
                MessageBox.Show("投注有誤");
            }
            else
            {
                labelShow.Text = (int.Parse(labelShow.Text) - numericUpDown1.Value) + "";
                button1.BackgroundImage =new Bitmap("down.jpg");
                timer1.Enabled = true;
                numericUpDown1.Enabled = false;
            }
        }
        int[] show = new int[4];
        int a = 0;
        private void timer1_Tick(object sender, EventArgs e)
        {
            button1.Enabled = false;
            Random rnd = new Random();
            int n1=rnd.Next(0,4);
            pictureBox1.BackgroundImage = new Bitmap(n1 + ".jpg");
            int n2 = rnd.Next(0, 4);
            pictureBox2.BackgroundImage = new Bitmap(n2 + ".jpg");
            int n3 = rnd.Next(0, 4);
            pictureBox3.BackgroundImage = new Bitmap(n3 + ".jpg");
            a++;
            if (a == 30)
            {
                timer1.Enabled = false;
                a = 0;
                button1.BackgroundImage = new Bitmap("up.jpg");
                if (n1 == 0 && n2 == 0 && n3 == 0)
                {
                    labelShow.Text = (int.Parse(labelShow.Text) + numericUpDown1.Value * 2).ToString();
                    MessageBox.Show("恭喜中獎,獎金x2倍!");
                }
                if (n1 == 1 && n2 == 1 && n3 == 1)
                {
                    labelShow.Text = (int.Parse(labelShow.Text) + numericUpDown1.Value * 10).ToString();
                    MessageBox.Show("恭喜中獎,獎金x10倍!");
                }
                if (n1 == 2 && n2 == 2 && n3 == 2)
                {
                    labelShow.Text = (int.Parse(labelShow.Text) + numericUpDown1.Value * 15).ToString();
                    MessageBox.Show("恭喜中獎,獎金x15倍!");
                }
                if (n1 == 3 && n2 == 3 && n3 == 3)
                {
                    labelShow.Text = (int.Parse(labelShow.Text) + numericUpDown1.Value * 20).ToString();
                    MessageBox.Show("恭喜中獎,獎金x20倍!");
                }
                button1.Enabled = true;
                numericUpDown1.Enabled = true;
            }
        }
    }
}
